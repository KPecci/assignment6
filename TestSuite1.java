import org.junit.*;
import static org.junit.Assert.*;

//This is a JUnit 4 Example

public class TestSuite1 {

  CheckSSN check1 = new CheckSSN();
  private int testnum = 0;

  @BeforeClass
  public static void beforeClass() {
    System.out.println("Test Suite 1 Tests Begin"); 
  }

  @AfterClass
  public static void afterClass() {
    System.out.println("Test Suite 1 Tests End");
  }

  //Properly Working case
  @Test
  public void test1() {
    assertEquals(true, check1.check_ssn("111-22-3333"));
  }

  //Check if format needs to be right
  @Test
  public void test2() {
    assertEquals(false, check1.check_ssn("-12-54-8-8"));
  } 

  //Check if you need any dashes or the right amount of numbers
  @Test
  public void test3() {
    assertEquals(false, check1.check_ssn("12345678"));
  } 

  //Check if you need numbers at all
  @Test
  public void test4() {
    assertEquals(false, check1.check_ssn("-"));
  } 

  //Check if you need anything
  @Test
  public void test5() {
    assertEquals(false, check1.check_ssn(""));
  } 
 

}
